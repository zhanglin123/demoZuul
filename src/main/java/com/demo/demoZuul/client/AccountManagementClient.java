package com.demo.demoZuul.client;

import com.daimler.otr.authentication.JwtUser;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "account-management.name")
public interface AccountManagementClient {

    @RequestMapping(method = RequestMethod.GET, path="/api/user-privileges")
    JwtUser getUserPrivilege(@RequestParam("token") String token);
}