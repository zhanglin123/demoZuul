package com.demo.demoZuul.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.context.annotation.Configuration;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

@Configuration
public class PreDecoration extends ZuulFilter {

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 7;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        requestContext.addZuulRequestHeader("my_zuul_request_header", "my_zuul_request_header");
        requestContext.addZuulResponseHeader("my_zuul_response_header", "my_zuul_response_header");
        requestContext.addZuulRequestHeader("Authorization", "1f0cc7ad-00eb-467a-9732-67dd5e62ed78");

        return null;
    }
}
